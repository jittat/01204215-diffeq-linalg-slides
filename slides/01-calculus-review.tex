\documentclass[aspectratio=169]{beamer}

\usetheme{Frankfurt}
\usepackage{graphicx}
\usepackage{epsfig}

\usefonttheme[onlymath]{serif}
\newcommand\comment[1]{}

\title{Calculus Review}
\subtitle{01204215 Differential Equations and Linear Algebra}
\author{Jittat Fakcharoenphol}
\institute{Kasetsart University}
\date{\today}

\begin{document}
\frame{\titlepage}

\section[Outline]{}
\frame{
  \frametitle{Outline}
  \tableofcontents
}

\frame{
  \frametitle{Quick notes}
  \begin{itemize}
  \item This is a quick review of calculus.
  \item The goal is to let you see the basic tools that we will use.
    We do not expect you to remember all these and it is ok if you
    have to look back at these materials.
  \item We would try to avoid tricky parts as much as possible, but
    familiarity with the materials helps you a lot when going through
    new topics.
  \item {\em Surely}, you can use tools like Wolfram alpha to help you
    do integrations or differentiation.
  \item The material here is based on Gilbert Strang's book:
    ``Differential Equations and Linear Algebra'', Wellesley-Cambridge
    Press, 2014.
  \end{itemize}
}

\section{Derivatives of key functions}
\frame{
  \frametitle{Derivatives of key functions}
  \begin{itemize}
  \item Polynomials: $x^n$
  \item Trigonometry: $\sin x$ and $\cos x$
  \item Exponential functions: $e^x$
  \item Logarithms: $\ln x$
  \end{itemize}
}

\frame{
  \frametitle{First principle}

  \[
  \frac{d}{dx} f(x) =
  \lim_{\Delta x\rightarrow 0}\frac{f(x+\Delta x) - f(x)}{\Delta x}
  \]
}

\frame{
  \frametitle{Practice}

  \[
  \frac{d}{dx} x^2 =
  \pause
  \lim_{h\rightarrow 0}\frac{(x+h)^2 - x^2}{h} 
  \]
}

\frame{
  \frametitle{Polynomials: $x^n$}
  \begin{eqnarray*}
    \frac{d}{dx} x^n &=& \lim_{\Delta x\rightarrow 0}\frac{(x+\Delta x)^n - x^n}{\Delta x}\\
    &=& \lim_{\Delta x\rightarrow 0}
    \frac{\left[x^n+\binom{n}{1}x^{n-1}\Delta x+\binom{n}{2}x^{n-2}(\Delta x)^2+\cdots\right] - x^n}{\Delta x}\\
    &=& \lim_{\Delta x\rightarrow 0}
    \frac{\left[\binom{n}{1}x^{n-1}\Delta x+\binom{n}{2}x^{n-2}(\Delta x)^2+\cdots+(\Delta x)^n\right]}{\Delta x}\\
    &=& \lim_{\Delta x\rightarrow 0}
    nx^{n-1}+\binom{n}{2}x^{n-2}\Delta x+\cdots+(\Delta x)^{n-1}\\
    &=& nx^{n-1}.
  \end{eqnarray*}
}

\frame{
  \frametitle{Exponential function: $e^x$}
  What exactly is $e$?
  \pause

  \[
  e=\sum_{n=0}^\infty \frac{1}{n!}.
  \]
  \pause

  Also,
  \[
  e=\lim_{n\rightarrow\infty}\left(1+\frac{1}{n}\right)^n.
  \]
}

\frame{
  \frametitle{Exponential function: $e^x$}
  \begin{eqnarray*}
    \frac{d}{dx} e^x &=& \lim_{\Delta x\rightarrow 0}\frac{e^{(x+\Delta x)} - e^x}{\Delta x}\\
    &=& \lim_{\Delta x\rightarrow 0}
    \frac{e^x\cdot e^{\Delta x} - e^x}{\Delta x}\\
    &=& \lim_{\Delta x\rightarrow 0}
    e^x\cdot\frac{e^{\Delta x} - 1}{\Delta x}\\
    &=& \lim_{\Delta x\rightarrow 0}
    e^x\cdot\frac{e^{\Delta x} - 1}{\Delta x}\\
    &=& \left(\lim_{\Delta x\rightarrow 0} e^x\right)\cdot\left(\lim_{\Delta x\rightarrow 0} \frac{e^{\Delta x} - 1}{\Delta x}\right)\\
    &=& e^x\cdot\left(\lim_{\Delta x\rightarrow 0} \frac{e^{\Delta x} - 1}{\Delta x}\right)\\
  \end{eqnarray*}
}

\frame{
  \frametitle{Natural logarithm: $\ln x$}
  \begin{eqnarray*}
    \frac{d}{dx} \ln x &=& \pause
    \lim_{h \rightarrow 0}\frac{\ln(x+h)-\ln x}{h}\\
    \pause
    &=& \lim_{h \rightarrow 0}\frac{\ln\left(\frac{x+h}{x}\right)}{h}\\
    &=& \lim_{h \rightarrow 0}\ln\left(\frac{x+h}{x}\right)^{1/h}\\
    &=& \lim_{h \rightarrow 0}\frac{x}{x}\cdot\ln\left(\frac{x+h}{x}\right)^{1/h}\\
    &=& \lim_{h \rightarrow 0}\frac{1}{x}\cdot\ln\left(\frac{x+h}{x}\right)^{x/h}
    = \frac{1}{x}\cdot \lim_{h \rightarrow 0}\ln\left(1 + \frac{h}{x}\right)^{x/h}\\
    &=& \frac{1}{x}\cdot \ln\left(\lim_{h \rightarrow 0}\left(1 + \frac{h}{x}\right)^{x/h}\right)
    = \frac{1}{x}\cdot \ln e = \frac{1}{x}.
  \end{eqnarray*}
}

\section{Rules for derivatives}
\frame{
  \frametitle{Rules for derivatives}
  \begin{itemize}
  \item Sum rule
  \item Product rule
  \item Quotient rule
  \item Chain rule
  \end{itemize}
}

\frame{
  \frametitle{Sum rule: $(f+g)'=f' + g'$}
  \begin{block}{}
    \[
    \frac{d}{dx}(f(x)+g(x)) =
    \frac{d}{dx}f(x)+
    \frac{d}{dx}g(x)
    \]
  \end{block}
  \begin{proof}
    \pause
    \begin{eqnarray*}
      \frac{d}{dx}(f(x)+g(x)) &=&
      \lim_{h\rightarrow 0}\frac{(f(x+h)+g(x+h)) - (f(x)+g(x))}{h}\\
      \pause
      &=& \lim_{h\rightarrow 0}\frac{f(x+h)-f(x)}{h} + 
      \lim_{h\rightarrow 0}\frac{g(x+h)-g(x)}{h} \\
      &=& 
      \frac{d}{dx}f(x)+
      \frac{d}{dx}g(x)
    \end{eqnarray*}
  \end{proof}
}

\frame{
  \frametitle{Product rule: $(f\cdot g)'=f\cdot g' + g\cdot f'$}
  \begin{block}{}
    \[
    \frac{d}{dx}(f(x)\cdot g(x)) =
    f(x)\cdot \frac{d}{dx}g(x) +
    g(x)\cdot \frac{d}{dx}f(x)
    \]
  \end{block}
  \begin{proof}
    {\small
    \begin{eqnarray*}
      \lefteqn{\frac{d}{dx}(f(x)\cdot g(x))
      \pause
      = \lim_{h\rightarrow 0}\frac{f(x+h)\cdot g(x+h) - f(x)\cdot g(x)}{h}}\\
      \pause
      &=& \lim_{h\rightarrow 0}
      \frac{
        f(x+h)\cdot g(x+h)
        -f(x+h)\cdot g(x)
        +f(x+h)\cdot g(x)
        - f(x)\cdot g(x)
      }{h}\\
      &=& \lim_{h\rightarrow 0}
      \frac{
        f(x+h)\cdot g(x+h)
        -f(x+h)\cdot g(x)
      }{h}
      + \lim_{h\rightarrow 0}
      \frac{
        f(x+h)\cdot g(x)
        - f(x)\cdot g(x)
      }{h}\\
      &=& \lim_{h\rightarrow 0}
      f(x+h)\cdot
      \frac{
        g(x+h) - g(x)
      }{h}
      + \lim_{h\rightarrow 0}
      g(x)\cdot
      \frac{
        f(x+h)
        - f(x)
      }{h}\\
      &=&
      f(x)\cdot \frac{d}{dx}g(x) +
      g(x)\cdot \frac{d}{dx}f(x)
    \end{eqnarray*}
    }
  \end{proof}
}

\frame{
  \frametitle{Quotient rule and Chain rule}

  \begin{block}{Chain rule}
    If $h(x)=f(g(x))$ then
    \[
    h'(x) = f'(g(x))g'(x).
    \]
    Or:
    \[
    \frac{dz}{dx} = \frac{dz}{dy}\cdot\frac{dy}{dx}
    \]
  \end{block}

  \begin{block}{Quotient rule}
    If $h(x)=f(x)/g(x)$, then
    \[
    h'(x) = \frac{f'(x)g(x)-f(x)g'(x)}{g(x)^2}.
    \]
  \end{block}
}

\section{The Fundamental Theorem of Calculus}
\frame{
  \frametitle{Antiderivatives}
  If the derivative of a function $F(x)$ with respect to $x$ is $f(x)$, i.e.,
  \[
  \frac{d}{dx}F(x) = f(x),
  \]
  we say that $F$ is an {\bf antiderivative} of $f$, and write
  \[
  \int f(x) dx = F(x).
  \]
  We also refer to $F$ as an {\bf indefinite integral} of $f$.  Since adding
  a constant does not change the derivative, we typically see an
  additional constant term $C$.

  \vspace{0.2in}
  
  Example: Find $\int (x^3+x^2)dx $.

  \vspace{0.3in}
}

\frame{
  \frametitle{Definite integrals}

  Suppose that we have a function $f(x)$, an integral of $f(x)$ with
  respect to $x$ on an interval $[a,b]$ is written as
  \[
  \int_a^b f(x)dx.
  \]

  \vspace{3in}
}

\frame{
  \frametitle{The Fundamental Theorem of Calculus}

  \begin{block}{}
    The derivative of the integral of $f(x)$ is $f(x)$.
  \end{block}

  \pause

  \begin{block}{}
  \[
  \int_a^b \frac{d}{dx}f(x)dx = f(b) - f(a).
  \]
  \end{block}

  \begin{block}{}
  \[
  \frac{d}{dx} \int_a^x f(s)ds = f(x).
  \]
  \end{block}
}

\frame{
  One way to get some idea.  Let $b=a+n\Delta x$.  We have
  {\small
    \begin{eqnarray*}
      f(b)-f(a) &=& f(a+n\Delta x)-f(a) \pause \\
      &=& \left[f(a+n\Delta x) - f(a+(n-1)\Delta x)\right] + \left[f(a+(n-1)\Delta x) - f(a)\right] \pause \\
      &=&
      [f(a+n\Delta x) - f(a+(n-1)\Delta x)] + \\
      &&
      [f(a+(n-1)\Delta x) - f(a+(n-2)\Delta x)] + \cdots \\
      &&
      [f(a+2\Delta x) - f(a+\Delta x)] +
      [f(a+\Delta x) - f(a)]
    \end{eqnarray*}
  }
  \pause
  {\small
    \[
    \left[
      \frac{f(a+\Delta x) - f(a)}{\Delta x} +
      \frac{f(a+2\Delta x) - f(a+\Delta x)}{\Delta x} +
      \frac{f(a+3\Delta x) - f(a+2\Delta x)}{\Delta x} + \cdots +
      \right.
      \]
      \[
      \left.
      \frac{f(a+n\Delta x) - f(a+(n-1)\Delta x)}{\Delta x}
      \right]
    \Delta x = f(a+n\Delta x) - f(a).
    \]
  }
  \vspace{2in}
}

\section{Taylor expansion}
\frame{
  \frametitle{Approximating function $f$}

  Suppose that you know something about $f(x)$ at point $x=0$, can you
  approximate the value of $f(x)$?

  If you know $f(0)$:

  \vspace{0.5in}
  \pause

  If you know $f'(0)$:

  \vspace{0.5in}
  \pause

  If you know $f''(0)$:

  \vspace{0.5in}
  \pause

  If you know $f'''(0)$:
  
}

\frame{
  \frametitle{Polynomial $f$}
  Suppose that $f(x)= \ \ \ \ \ \ \ \ \  a_2x^2 + a_1x + a_0$.

  Approximating with $f(0)$ would just give you: \pause

  What is $f'(0)$? \pause
  \vspace{0.5in}

  What is $f''(0)$? \pause
  \vspace{0.5in}

  What is $f'''(0)$? 
  \vspace{0.5in}
}

\frame{
  \frametitle{Polynomial $f$}
  If $f$ is a polynomial of degree $k$, we would have that
  \[
  f(x) = f(0) +
  \frac{f'(0)x}{1!} + 
  \frac{f''(0)x^2}{2!} + 
  \frac{f'''(0)x^3}{3!} + \cdots
  \frac{f^{(k)}(0)x^k}{k!}.
  \]
}

\frame{
  \frametitle{Taylor expansion}

  We can treat any function $f$ as if it is a polynomial (for
  approximation purpose).

  Thus we would approximate $f(x)$ (around point $0$) with
  \[
  f(0) +
  \frac{f'(0)x}{1!} + 
  \frac{f''(0)x^2}{2!} + 
  \frac{f'''(0)x^3}{3!} + \cdots
  \frac{f^{(k)}(0)x^k}{k!} + \cdots
  \]

  In general, we can do that around any point $a$.  This gives
  \[
  f(x)\approx
  f(a) +
  \frac{f'(a)(x-a)}{1!} + 
  \frac{f''(a)(x-a)^2}{2!} + 
  \frac{f'''(a)(x-a)^3}{3!} + \cdots
  \frac{f^{(k)}(a)(x-a)^k}{k!} + \cdots
  \]
}

\frame{
  \frametitle{Taylor expansion of $e^x$}

  \[
  e^x = \sum_{n=0}^\infty \frac{x^n}{n!}
  = 1 + x + \frac{x^2}{2!} + \frac{x^3}{3!} + \cdots.
  \]

  \vspace{0.5in}
  \noindent
  $\sin x =$

  \vspace{0.5in}
  \noindent
  $\cos x =$
  \vspace{0.5in}
}

\end{document}
