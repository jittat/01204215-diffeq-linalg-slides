\include{commons}
\lecturetitle{Lecture 11b: Orthogonality} 

\frame{
  \frametitle{Motivation}
  \begin{itemize}
  \item Vector spaces capture basic properties of ``spaces'', i.e.,
    linear scaling and addition, and how one could ``combine'' vectors
    from the vector spaces.
  \item What else do $\rf^n$ or $\cf^n$ have?  \pause
  \item The notion of distances.  We can tell how ``close'' two
    vectors are.
  \item In this section, we will define notion of distances, called
    the norm or a vector $\uv$, denoted by $\|\uv\|$ and use it to
    deal with other properties such as orthogonality.
  \item {\em Notes: to simplify the lecture, we would mainly deal with
    $\rf$, ignoring the complex numbers $\cf$ for now.}
  \end{itemize}

}

\frame{
  \frametitle{Example}
}

\frame{
  \frametitle{Distances}
  \begin{itemize}
  \item The distance between vectors $\uv$ and $\vv$ is the ``length''
    of vector $\uv-\vv$.
  \item But how can we define ``lengths''?
  \item Mathematically, we use the term {\color{red}\bf norm} for the
    vector's length.  The norm of vector $\vv$ is denoted by
    $\|\vv\|$.  \pause
  \item Desired property: \pause
    \begin{itemize}
    \item (N1) $\|\vv\|$ is a non-negative real number,
      \pause
    \item (N2) $\|\vv\|$ is zero iff $\vv$ is a zero vector,
      \pause
    \item (N3) for any $\alpha$, $\|\alpha\vv\| = |\alpha|\cdot \|\vv\|$, and
      \pause
    \item (N4) $\|\uv+\vv\| \leq \|\uv\| + \|\vv\|$.  (Triangle inequality)
    \end{itemize}
    \pause
  \item We would define norms based on the other operation called the
    {\color{red}\bf inner product}.
  \end{itemize}
}

\frame{
  \frametitle{Inner product}

  An inner product is a function $\langle\cdot,\cdot\rangle:\V\times\V
  \longrightarrow \rf$, satisfying the following properties
  \begin{itemize}
  \item Symmetry: $\langle \uv,\vv \rangle = \langle \vv,\uv \rangle$,
  \item Linearity in the first argument: $\langle a\uv+b\vv,\vect{w}\rangle = a\langle \uv,\vect{w}\rangle + b\langle \vv,\vect{w}\rangle$, and
  \item Positive-definiteness: $\langle \uv,\uv\rangle > 0$ if
    $\uv\neq\vect{0}$.
  \end{itemize}

  \pause

  \begin{block}{Dot product}
    One way to define an inner product is to use the dot product.
    I.e., for $\uv=(u_1,u_2,\ldots,u_n)$ and
    $\vv=(v_1,v_2,\ldots,v_n)$, we let
    \[
    \langle \uv,\vv\rangle = \sum_{i=1}^n u_i\cdot v_i,
    \]
    or in matrix notation $\langle \uv,\vv \rangle=\uv^T\vv$.
  \end{block}
}

\frame{
  \frametitle{Inner product to norm}
  
  Given the definition of inner product, we can define the norm of
  $\vv$ to be
  \[
  \|\vv\| = \sqrt{\langle \vv,\vv \rangle}.
  \]

  \pause
  One can check that the desired properties is satisfied. \pause
  
  \begin{itemize}
  \item (N1) $\|\vv\|$ is a non-negative real number,
    \pause
  \item (N2) $\|\vv\|$ is zero iff $\vv$ is a zero vector,
    \pause
  \item (N3) for any $\alpha$, $\|\alpha\vv\| = |\alpha|\cdot \|\vv\|$, and
    \pause
  \item (N4) $\|\uv+\vv\| \leq \|\uv\| + \|\vv\|$.  (Triangle inequality)
  \end{itemize}
}

\frame{
  \frametitle{Orthogonality: Pythagorean theorem}
  
}

\frame{
  \frametitle{Orthogonality: pythagorean theorem - vector lengths}

  Given vectors $\uv=(u_1,\ldots,u_n)$ and $\vv=(v_1,\ldots,v_n)$,
  what is the condition that implies the Pythagorean theorem, i.e.,
  \[
  \|\uv\|^2 + \|\vv\|^2 = \|\uv-\vv\|^2.
  \]

  \pause

  Let's try to work it out.  The RHS becomes
  \[
  \|\uv-\vv\|^2 = \pause
  \sum_{i=1}^n (u_i-v_i)^2 = \pause
  \sum_{i=1}^n \left(u_i^2-2u_iv_i+v_i^2\right) = \pause
  \langle \uv,\uv\rangle + 
  \langle \vv,\vv\rangle - 2\langle \uv,\vv\rangle.
  \]

  \pause

  Thus, the condition that we need is that
  \[
  \uv^T\vv=
  \langle\uv,\vv\rangle = 0.
  \]
}

\frame{

  \begin{block}{Orthogonal vectors}
    Vector $\uv\in\rf^n$ is {\color{red}\bf orthogonal to} vector
    $\vv\in\rf^n$ iff
    \[
    \uv^T\vv = \langle \uv,\vv\rangle = 0.
    \]
  \end{block}
}

\frame{
  \frametitle{Orthogonal subspaces}

  \begin{block}{Definition}
    Consider two subspaces $\V$ and $\W$ of $\rf^n$.  If every vector
    in $\V$ is orthogonal to every vector in $\W$, we say that $\V$
    and $\W$ are {\color{red}\bf orthogonal}.
  \end{block}

  \pause

  Any examples?
}

\begin{frame}
  \frametitle{Review: Four fundamental subspaces}

  \begin{block}{Four fundamental subspaces}
    Given an $m$-by-$n$ matrix $A$, we have the following subspaces
    \begin{itemize}
    \item The column space of $A$ (denoted by ${\mathcal C}(A)$
      {$\subseteq \rf^m$}
      )
    \item The row space of $A$ (denoted by ${\mathcal C}(A^T)$
      {$\subseteq \rf^n$}
      )
    \item The nullspace of $A$
      \[
        {\mathcal N}(A) = \{\vect{x} \;|\; A\vect{x}=\vect{0}\}
        {\subseteq \rf^n}
      \]
    \item The left nullspace of $A$
      \[
        {\mathcal N}(A^T) = \{\vect{y} \;|\; A^T\vect{y}=\vect{0}\}
        {\subseteq \rf^m}
      \]
    \end{itemize}
  \end{block}
\end{frame}

\frame{
  \frametitle{Row spaces and nullspaces}

  \begin{lemma}
    Consider an $m\times n$-matrix $A$.  Its row space ${\mathcal
      C}(A^T)$ is orthogonal to its nullspace ${\mathcal N}(A)$.
  \end{lemma}
  \begin{proof}
    \pause

    Consider vector $\vect{x}\in {\mathcal C}(A^T)$ and
    $\vect{y}\in{\mathcal N}(A)$.  \pause We know that $\vect{x}$ can
    be written as a linear combination of rows of $A$; thus, there
    exists a vector $\uv\in\rf^m$ such that
    \[
    \vect{x}=(\uv^T A)^T=A^T\uv.
    \]
    \pause
    We also know that $A\vect{y}=\vect{0}$. (why?) \pause Combining
    this we have that
    \[
    \vect{x}^T\vect{y} = (\uv^T A)\vect{y} = \uv^T(A\vect{y})=\uv^T\vect{0}=0.
    \]
  \end{proof}
}

\frame{
  \frametitle{Examples}
  
}

\frame{

  \begin{block}{Definition: orthogonal complement}
    Given a subspace $\V$ of $\rf^n$, the subspace of all vectors
    orthogonal to $\V$ is called the {\color{red}\bf orthogonal
      complement} of $V$.  We denote it by $\V^{\perp}$.
  \end{block}
  
  \vspace{0.2in}
  
  We have that
  \[
    {\mathcal N}(A) = ({\mathcal C}(A^T))^\perp,
  \]
  and also
  \[
    {\mathcal C}(A^T) = ({\mathcal N}(A))^\perp.
    \]
}

\frame{
  \frametitle{The matrix and its subspaces}

}

\frame{
  \frametitle{What is a matrix?}

  \begin{block}{}
    Every matrix transforms its row space to its column space.
  \end{block}
  
}

\frame{
  \frametitle{Projection: example}
}


\frame{
  \frametitle{Cosines: example}
  
}

\frame{
  \frametitle{Inner products and cosines}
  \pause

  \[
  \cos \theta =
  \cos (\beta - \alpha) = \pause
  \cos\beta \cos\alpha + \sin\beta\sin\alpha = \pause
  \frac{a_1b_1+a_2b_2}{\|\vect{a}\|\|\vect{b}\|}.
  \]

  \pause

  \begin{block}{}
    \[
    \cos\theta = \frac{\vect{a}^T\vect{b}}{\|\vect{a}\|\|\vect{b}\|}
    \]
  \end{block}
}

\frame{
  \frametitle{Projection onto a line}

  Let's first look at the figure.
  \pause

  We know that the projection point $\vect{p}=\alpha\vect{a}$, for
  some $\alpha$.  \pause
  We also know that $\vect{b}-\vect{p}$ is orthogonal
  to $\vect{a}$; thus,
  \[
  (\vect{b}-\alpha\vect{a})^T\vect{a} = 0,
  \]
  \pause
  i.e.,
  \[
  \alpha = \frac{\vect{b}^T\vect{a}}{\vect{a}^T\vect{a}}.
  \]
  \pause
  We have that
  \[
  \vect{p} = \frac{\vect{b}^T\vect{a}}{\vect{a}^T\vect{a}}\vect{a}.
  \]

  \pause

  {\bf Special case:} when $\vect{a}$ is a unit vector (i.e.,
  $\|a\|=1$), $\vect{p} = (\vect{b}^T\vect{a})\vect{a}.$

}

\frame{
  \frametitle{A good basis}
}

\frame{
  \frametitle{Orthonormal basis}

  A basis is {\bf orthogonal} is every vector in it is orthogonal to
  every other vector.

  An orthogonal basis is {\bf orthonormal} if every vector also has
  unit-length.

}

\frame{
  \frametitle{Orthonormal basis}

  The vectors $\uv_1,\ldots,\uv_n$ are {\color{red}\bf orthonormal} if
  \[
  \uv_i^T\uv_j =
  \left\{
  \begin{array}{ll}
    0 & \mbox{if $i\neq j$} \\
    1 & \mbox{if $i=j$}.
  \end{array}
  \right.
  \]
}

\frame{
  \frametitle{Building orthonormal basis}

  \begin{itemize}
  \item Scale vectors (normalize)
  \item Removing components of other vectors (using projection)
  \end{itemize}
}

\frame{
  \frametitle{Gram-Schmidt procedure}
}

\frame{
  \frametitle{QR factorization}

  \[
  A = QR,
  \]
  where $Q$ is orthonormal and $R$ is upper-right triangular.
}
