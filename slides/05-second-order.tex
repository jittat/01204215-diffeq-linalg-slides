\documentclass[aspectratio=169]{beamer}

\usetheme{Boadilla}

\usepackage{graphicx}
\usepackage{epsfig}
\usepackage{bm}

\usefonttheme[onlymath]{serif}
\newcommand\comment[1]{}

\newcommand{\vect}[1]{\bm{#1}}

\title{Second-Order Linear Differential Equations}
\subtitle{01204215 Differential Equations and Linear Algebra}
\author{Jittat Fakcharoenphol}
\institute{Kasetsart University}
\date{\today}

\begin{document}
\frame{\titlepage}

\frame{
  \frametitle{Motivation}
}

\frame{
  \frametitle{Circuit analysis: RCL circuit}
  \pause

  Components:
  \begin{itemize}
  \item Resistors: \pause $V_R=RI$. \pause
  \item Capacitor:
    \[
    Q'=I, \ \ \ \pause V_C=\frac{1}{C}Q,
    \]
    where $C$ is the capacitance. \pause
  \item Inductor: \pause
    \[
    V_L = LI',
    \]
    where $L$ is the inductance.
  \end{itemize}
}

\frame{
  \frametitle{Circuit analysis: RCL circuit}

  We have $V_L+V_R+V_C = E(t)$, or \pause
  \[
  LI'+RI+\frac{1}{C}{Q} = E(t).
  \]
  \pause
  However, since $Q'=I$, we have
  \[
  LQ''+RQ'+\frac{1}{C}{Q} = E(t).
  \]
  Dividing by $L$ and differentiate, we get
  \[
  I''+\frac{R}{L}I'+\frac{1}{LC}{I} = \frac{1}{L}E'(t).
  \]
  
}

\frame{
  \frametitle{Mechanics}
}

\frame{
  \frametitle{Homogeneous and nonhomogeneous equations}

  \[
  ax'' + bx' + cx = f(t)
  \]

  \[
  ax'' + bx' + cx = 0
  \]

  \pause

  Initial Value Problem (IVP) also comes with initial conditions
  $x(0)=x_0$ and $x'(0) =x_1$.

}

\frame{
  \frametitle{Homogeneous equations}

  Consider two solutions $x_1(t)$ and $x_2(t)$ to a homogeneous
  equation
  \[
  ax'' + bx' + cx = 0
  \]

  \vspace{2in}
}

\frame{
  \frametitle{Independent solutions}

  If the equation has constant coefficients, there are always two
  indepedent soltutions $x_1(t)$ and $x_2(t)$ (called basis, or {\bf
    fundamental set}).  Any linear combination of $x_1(t)$ and
  $x_2(t)$
  \[
  c_1x_1(t) + c_2x_2(t)
  \]
  is also a solution to the homogeneous equation.  This is called the
  {\bf general solution}, as it contains all IVPs solutions.
  
}

\frame{
  \frametitle{Existance}
  Consider a homogeneous equation
  \[
  ax'' + bx' + cx = 0.
  \]
  Let $x_1(t)$ be a solution satifying
  \[
  x_1(0)=1, x'_1(0) = 0,
  \]
  and let $x_2(t)$ be a solution satisfying
  \[
  x_2(0)=0, x'_2(0) = 1.
  \]
  \pause

  Remark: $x_1$ and $x_2$ are linearly independent.
  
  Consider an IVP with conditions $x(0)=A$ and $x'(0)=B$. \pause We
  know that one solution that satisfies this initial condition is
  \[
  Ax_1(t) + Bx_2(t).
  \]
}
  
  

\frame{
  \frametitle{Uniqueness}

  \begin{theorem}
    The IVP problem has a unique solution that exists on $-\infty < t
    < \infty$.
  \end{theorem}

  \pause
  
  Thus our linear combination solution to condition $x(0)=A,x'(0)=B$
  \[
  Ax_1(t) + Bx_2(t).
  \]
  is the unique solution.
}

\frame{
  \frametitle{Finding general solutions}

  Consider the equation
  \[
  ax'' + bx' + cx = 0.
  \]
  \pause
  
  Since the sum is equal to zero, $x''$, $x'$, and $x$ must be
  functions of the same form.  \pause
  So we guess that $x(t)=e^{\lambda t}$
  for some $\lambda$.  \pause Substitute that into the equation, we get
  \[
  a\lambda^2 e^{\lambda t} +
  b\lambda e^{\lambda t} +
  c e^{\lambda t}  = 0.
  \]
  \pause
  We can cancel out $e^{\lambda t}$ because it is always non-zero to
  yield
  \begin{block}{}
  \[
  a\lambda^2 + b\lambda + c = 0.
  \]
  \end{block}
  This is call the {\bf characteristic equation} of the original
  equation.

  We can now use the standard formula to find $\lambda$, i.e., the
  formula
  \[
  \frac{-b \pm \sqrt{b^2-4ac}}{2a}.
  \]
}
  
\frame{
  \frametitle{3 cases}
  Using the formula
  \[
  \frac{-b \pm \sqrt{b^2-4ac}}{2a}.
  \]
  to find the roots of the characteristic equation, we end up in three
  possible cases.
  \pause

  \begin{itemize}
  \item {\bf Two different real roots.} \pause
  \item {\bf Two copies of one real root.} \pause
  \item {\bf Two complex roots.} 
  \end{itemize}
}

\frame{
  \frametitle{Two real roots}

  Consider
  \[
  x''-x'-12x = 0.
  \]
  \pause
  We have the following characteristic equation
  \[
  \lambda^2-\lambda-12=0,
  \]
  or
  \[
  (\lambda+3)(\lambda-4)=0.
  \]
  Thus, the roots are $-3,4$.  The fundamental set of solutions is
  \[
  x_1(t)=e^{-3t}, \ \ \ x_2(t)=e^{4t},
  \]
  and the general solution is
  \[
  x(t)=c_1e^{-3t}+c_2e^{4t}.
  \]
}

\frame{
  \frametitle{One real root}

  Consider
  \[
  x''+4'+4x = 0.
  \]
  \pause
  We have the following characteristic equation
  \[
  \lambda^2+4\lambda+4=0,
  \]
  or
  \[
  (\lambda+2)(\lambda+2)=0.
  \]
  Thus, the roots are $-2,-2$.  The fundamental set of solutions is
  \[
  x_1(t)=e^{-2t}, \ \ \ \pause x_2(t)=te^{-2t},
  \]
  and the general solution is
  \[
  x(t)=c_1e^{-2t}+c_2te^{-2t}.
  \]
}

\frame{
  \frametitle{Two complex roots}

  When $b^2-4ac < 0$, the formula
  \[
  \frac{-b \pm \sqrt{b^2-4ac}}{2a}.
  \]
  gives two complex roots of the forms
  \[
  \alpha + i\beta, \ \ \ \ \alpha - i\beta,
  \]
  for some reals $\alpha$ and $\beta$.

  \pause

  We can try to plug in to obtain solutions of the form
  \[
  e^{(\alpha+i\beta)t}.
  \]
}

\frame{
  \frametitle{Euler's formula}

  \begin{block}{Euler's formula}
    \[
    e^{i\beta t}
    = \cos \beta t + i\cdot\sin \beta t
    \]
  \end{block}

  Using that we know that
  \[
  e^{(\alpha+i\beta)t}=e^{\alpha t}(\cos \beta t + i\cdot\sin \beta t).
  \]
  \pause
  However, we want a fundamental set of two real solutions.
}

\frame{
  \frametitle{Complex solutions}

  \begin{theorem}
    If $x(t)=g(t) + i h(t)$ is a complex-valued solution to the
    different equation
    \[
    ax''+bx''+c=0.
    \]
    Then its real and imaginary parts $x_1(t)=g(t)$ and $x_2(t)=h(t)$
    are real-valued solutions.
  \end{theorem}

  \pause

  Using this theorem, we know that the fundamental set of solutions
  would be
  \[
  x_1(t) = e^{\alpha t}\cos \beta t,
  \]
  and
  \[
  x_2(t) = e^{\alpha t}\sin \beta t.
  \]
  
}

\frame{
  \frametitle{Two complex roots}

  Consider
  \[
  x''+2x'+5x = 0.
  \]
  \pause
  We have the following characteristic equation
  \[
  \lambda^2+2\lambda+5=0.
  \]
  \pause
  The roots are $-1\pm 2i$.  The fundamental set of solutions is
  \[
  x_1(t)=e^{-t}\cos 2t, \ \ \ x_2(t)=e^{-t}\sin 2t,
  \]
  and the general solution is
  \[
  x(t)=c_1e^{-t}\cos 2t +c_2e^{-t}\sin 2t.
  \]
}

\frame{
  \frametitle{Pure imaginary roots}

  Consider the IVP
  \[
  x''+7x = 0,
  \]
  with $x(0)=1, x'(0)=2$.
  \pause
  We have the following characteristic equation
  \[
  \lambda^2+7=0.
  \]
  \pause
  The roots are $\pm \sqrt{7}i$.  The fundamental set of solutions is
  \[
  x_1(t)=\cos \sqrt{7}t, \ \ \ x_2(t)=\sin \sqrt{7}t,
  \]
  \pause
  and the general solution is
  \[
  x(t)=c_1\cos\sqrt{7}t + c_2\sin\sqrt{7}t.
  \]

  Plugging in $x(0)=1$, \pause we get $c_1=1$.  Note that
  \[
  x'(t) = - c_1\sqrt{7}\sin\sqrt{7}t + c_2\sqrt{7}\cos\sqrt{7}t
  \]
  and plugging in $x'(0)=2$, we have that \pause $c_2=2/\sqrt{7}$.
}

\frame{
  \frametitle{Summary}

  \begin{tabular}{|c|c|}
    \hline
    roots & general solution \\
    \hline
    & \\
    $\lambda_1\neq \lambda_2$ & \hspace{4in} \\
    & \\
    \hline
    & \\
    $\lambda_1 = \lambda_2$ & \\
    & \\
    \hline
    & \\
    $\lambda = \alpha \pm i\beta$ & \\
    & \\
    \hline
    & \\
    $\lambda = \pm i\beta$ & \\
    & \\
    \hline
  \end{tabular}
}

\frame{
  \frametitle{Applications}
}

\frame{
  \frametitle{Higher-order homogeneous linear differential equations}

  Consider homogeneous linear differential equation
  \[
  a_nx^{(n)} + a_{n-1}x^{(n-1)} + a_{n-2}x^{(n-2)}+\cdots+a_1x' +a_0x=0.
  \]
  There are $n$ linearly independent solutions
  $x_1(t),x_2(t),\ldots,x_n(t)$such that any IVP solution is a linear
  combination of $x_1(t),x_2(t),\ldots,x_n(t)$.
}

\frame{
  \frametitle{Nonhomogeneous Equations}
  \[
  ax''+bx'+cx = f(t)
  \]
}

\frame{
  \frametitle{Nonhomogeneous Equations as Systems}
}

\frame{
  \frametitle{Review: a system of linear equations}

  Homogeneous linear system
  \[
  A\vect{x} = \vect{0}
  \]

  Nonhomogeneous linear system
  \[
  A\vect{x} = \vect{b}
  \]
  
  \pause
  
  The set of solutions $\{\vect{x} \;|\; A\vect{x}=\vect{0}\}$ is a
  {\bf vector space}.

  The set of solutions $\{\vect{x} \;|\; A\vect{x}=\vect{b}\}$ is an
  {\bf affine space}, which equals
  \[
  \vect{p} + \{\vect{x} \;|\; A\vect{x}=\vect{0}\},
  \]
  where $\vect{p}$ is any solution to the nonhomogeneous system, i.e.,
  $A\vect{p}=\vect{b}$.
  
}

\frame{
  \frametitle{General solutions: nonhomogeneous equations}

  Consider equation
  \[
  ax''+bx'+cx = f(t).
  \]

  The general solution to the equation is
  \[
  x(t)=c_1x_1(t) + c_2x_2(t) + x_p(t),
  \]
  where $x_1(t)$ and $x_2(t)$ are linearly independent solution to
  \[
  ax''+bx'+cx = 0,
  \]
  and $x_p(t)$ is any specific solution to the equation.
  \pause

  We call
  \[
  x_h(t) = c_1x_1(t) + c_2x_2(t)
  \]
  the {\bf homogeneous solution}, and $x_p(t)$ the {\bf particular
    solution}.

}

\frame{
  \frametitle{The method of undetermined coefficients}

  How can one find a particular solution to
  \[
  ax'' + bx' + cx = f(t).
  \]

  \pause

  Guess and try.
  
  \begin{tabular}{|c|c|}
    \hline
    $f(t)$ & forms of $x_p(t)$ \\
    \hline
    $\alpha$ & $A$ \\
    $\alpha e^{\beta t}$ & $A e^{\beta t}$ \\
    polynomial degree $n$ & $A_nt^n+\cdots+A_1 t + A_0$ \\
    $\alpha\sin \omega t$; $\alpha\cos \omega t$ & 
    $A\sin \omega t + B\cos \omega t$ \\
    $\alpha e^{rt}\sin \omega t$; $\alpha e^{rt}\cos \omega t$ & 
    $e^{rt}(A\sin \omega t + B\cos \omega t)$ \\
    \hline
  \end{tabular}

  {\bf Careful:} If the particular solution found is duplicated with
  one of the fundametal solutions, we need to modify it.

}

\frame{
  \frametitle{Example 1}
  Find the general solution to
  \[
  x''+3x=4e^{-5t}.
  \]

  \vspace{2.5in}
}

\frame{
  \frametitle{Example 2}
  Find the general solution to
  \[
  x''-x'+7x=5t-3.
  \]

  \vspace{2.5in}
}

\frame{
  \frametitle{Example 3}
  Find the general solution to
  \[
  x''-x=5e^{-t}.
  \]

  \vspace{2.5in}
}
\end{document}
