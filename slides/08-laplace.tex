\documentclass[aspectratio=169]{beamer}

\usetheme{Boadilla}

\usepackage{graphicx}
\usepackage{epsfig}
\usepackage{bm}

\usefonttheme[onlymath]{serif}
\newcommand\comment[1]{}

\newcommand{\vect}[1]{\bm{#1}}
\newcommand{\Lp}{{\mathcal L}}

\title{The Laplace Transform and Differential Equations}
\subtitle{01204215 Differential Equations and Linear Algebra}
\author{Jittat Fakcharoenphol}
\institute{Kasetsart University}
\date{\today}

\begin{document}
\frame{\titlepage}

\frame{
  \frametitle{Motivation}
}

\frame{
  \frametitle{Using the Laplace transform to solve differential equations}
}

\frame{

  {\bf Demo.}
  Let $k$ be a constant.  Consider the following IVP
  \[
  x''+k^2x=0, \ \ \ \ x(0)=0, \ \ \ \ x'(0)=1.
  \]
  \pause
  We take the Laplace transform of the function to obtain
  \[
  \Lp[x''+k^2x]=\Lp[x'']+\Lp[k^2 x]=\Lp[0]=0.
  \]
  \pause
  We later show that if $\Lp[x]=X(s)$,
  \[
  \Lp[x''] = s^2X(s)-sx(0)-x'(0),
  \]
  then the equation becomes
  \[
  s^2X(s) - sx(0) - x'(0) - k^2X(s) = 0,
  \]
  \pause
  which solves to
  \[
  X(s) = \frac{1}{k^2+s^2}=\frac{1}{k}\cdot \frac{k}{s^2+k^2}.
  \]
  \pause
  We perform the inverse transform to get that
  \[
  x(t)=\frac{1}{k}\sin kt.
  \]
}

\frame{
  \frametitle{Definition}

  \begin{block}{}
    Let $x(t)$ be a function defined on interval $0\leq t < \infty$.
    The {\bf Laplace transform} of $x(t)$ is the function $X(s)$
    defined by
    \[
    X(s) = \int_0^\infty x(t)e^{-st} dt,
    \]
    when the improper integral exists.  We also write
    \[
    \Lp[x]=X(s)
    \]
  \end{block}

  \vspace{0.1in}
  
  {\small Remark: the improper integral $\int_0^\infty f(t)dt$ is
    defined to be
    \[
    \lim_{b\rightarrow\infty} \int_0^b f(t)dt.
    \]
  }
}

\frame{
  \frametitle{Linearity}

  \begin{block}{}
    For constant $a$ and $b$,
    \[
    \Lp[a\cdot x(t) + b\cdot y(t)] =
    a\cdot\Lp[x(t)] + b\cdot\Lp[y(t)].
    \]
  \end{block}

  {\bf Proof.} (idea)

  Note that
  \[
  \int_0^\infty \left(
  a\cdot x(t) + b\cdot y(t)
  \right) e^{-st}dt = 
  a\cdot\int_0^\infty x(t)e^{-st}dt + 
  b\cdot\int_0^\infty y(t)e^{-st}dt
  \]
}

\frame{
  \frametitle{Constant functions}

  Let's derive $\Lp[1]$.  Let $x(t)=1$.  We have
  \begin{eqnarray*}
    X(s)
    &=& \int_0^\infty 1\cdot e^{-st}dt \\ \pause
    &=& \lim_{b\rightarrow\infty} \int_0^b e^{-st}dt \\ \pause
    &=& \lim_{b\rightarrow\infty} -\frac{1}{s} e^{-st} \bigg\rvert_0^b \\ \pause
    &=& \lim_{b\rightarrow\infty} \left[\frac{1}{s} e^{-s\cdot 0} - \frac{1}{s}e^{-s\cdot b} \right] \\
    &=& \frac{1}{s},
  \end{eqnarray*}
  when $s>0$.
}

\frame{
  \frametitle{Exponential functions}

  Let's derive $\Lp[e^{at}]$.  Let $x(t)=e^{at}$.  We have
  \begin{eqnarray*}
    X(s)
    &=& \int_0^\infty e^{at}\cdot e^{-st}dt 
    = \int_0^\infty e^{(a-s)t}dt  \\
    &=& \lim_{b\rightarrow\infty} \int_0^b e^{(a-s)t} dt \\
    &=& \lim_{b\rightarrow\infty}\frac{1}{(a-s)}e^{(a-s)t} \bigg\rvert_0^b \\
    &=& \lim_{b\rightarrow\infty}
    \left[
      \frac{1}{(a-s)}e^{(a-s)b} - 
      \frac{1}{(a-s)}e^{(a-s)\cdot 0}
      \right] \\
    &=& \frac{1}{s-a},
  \end{eqnarray*}
  when $s-a>0$, or $s>a$.
}

\frame{
  \frametitle{$\Lp[t]$}

  Let $x(t)=t$.  Let's derive $X(s)=\Lp[x(t)]=\Lp[t]$.  We want to
  compute
  \[
  \int_0^\infty te^{-st}dt.
  \]

  Let's do integration by parts.
  
}

\frame{
  \frametitle{Review: integration by parts}

  Recall that
  \[
  \frac{d}{dt} uv = u\frac{d}{dt} v + v \frac{d}{dt}u
  =uv' + vu'
  \]
  \pause
  Thus, 
  \[
  uv = \int u v' dt + \int v u' dt.
  \]
  \pause
  Rearranging, we get
  \begin{block}{}
    \[
    \int u v' dt = uv - \int v u' dt.
    \]
  \end{block}
}

\frame{
  \frametitle{$\Lp[t]$}

  Let $x(t)=t$.  Let's derive $X(s)=\Lp[x(t)]=\Lp[t]$.  We want to
  compute
  \[
  X(s)=
  \int_0^\infty t\cdot e^{-st}dt.
  \]

  Let's do integration by parts, i.e., $\int u v' dt = uv - \int v u'
  dt$.  Let $u=t$ and $v'=e^{-st}$, i.e., $v(t)=-\frac{1}{s}e^{-st}$,
  and $u'=1$.

  \pause
  We have
  \begin{eqnarray*}
    \int_0^\infty t\cdot e^{-st}dt
    &=& t\cdot\left(-\frac{1}{s}e^{-st}\right)\bigg\vert_0^\infty
    -
    \int_0^\infty \left(-\frac{1}{s}e^{-st}\right)\cdot 1 dt \\
    \pause
    &=& t\cdot\left(-\frac{1}{s}e^{-st}\right)\bigg\vert_0^\infty
    +\frac{1}{s}\left(
    -\frac{1}{s}e^{-st}
    \right)\bigg\vert_0^\infty\\
    \pause
    &=&
    \frac{1}{s^2},
  \end{eqnarray*}
  when $s>0$.
}

\frame{
  \frametitle{Existance of the transformed function}

  The transformed $\Lp[x]$ exists when the improper integration
  \[
  \int_0^\infty x(t)e^{-st} dt,
  \]
  converges.  This can only be true when $\lim_{t\rightarrow\infty}
  x(t)e^{-st} = 0$, i.e., $x$ cannot grow too fast.  We can write this
  condition as there exist $M>0$ and $r$ such that
  \[
  |x(t)| \leq Me^{rt},
  \]
  for all $t>t_0$ for some $t_0$.  Furthermore, we want $x(t)$ to be
  piecewise continuous (PWC).

}

\frame{
  \frametitle{Unit switching function}

  The {\bf Heaviside function} $H(t)$ is defined as
  \[
  H(t) = \left\{
  \begin{array}{cl}
    0, & t < 0 \\
    1, & t \geq 0
  \end{array}
  \right.
  \]

  The Laplace transform of $H(t)$ is
  \[
  \Lp[H(t)] =
  \int_0^\infty H(t)e^{-st}dt =
  \int_0^\infty 1\cdot e^{-st} dt = \Lp[1] = \frac{1}{s}.
  \]
}

\frame{
  \frametitle{Translated unit switching function}

  The function $H(t-a)$ is the translated version of the Heaviside
  function, i.e.,
  \[
  H(t-a) = \left\{
  \begin{array}{cl}
    0, & t < a \\
    1, & t \geq a
  \end{array}
  \right.
  \]

  The Laplace transform of $H(t-a)$, when $a\geq 0$, is
  \[
  \Lp[H(t-a)] =
  \int_0^\infty H(t-a)e^{-st}dt =
  \int_a^\infty 1\cdot e^{-st} dt = -\frac{1}{s}e^{-st}\bigg\vert_a^\infty
  = \frac{1}{s}e^{-as},
  \]
  when $s>0$.
}

\frame{
  \frametitle{Translation, in general}

  \begin{block}{Switching property}
    If $\Lp[f(t)] = F(s)$, then
    \[
    \Lp[H(t-a)f(t-a)] = e^{-as}F(s).
    \]
  \end{block}

  \vspace{0.2in}
  
  \begin{block}{Shift property}
    If $\Lp[f(t)] = F(s)$, then
    \[
    \Lp[f(t)e^{at}] = F(s-a).
    \]
  \end{block}
  
}

\frame{

  Consider the following function
  \[
  f(t)=
  \left\{
  \begin{array}{ll}
    3, & 0\leq t < 2\\
    4, & 2\leq t < 3\\
    2, & 3< t \leq 6\\
    0, & t > 6\\
  \end{array}
  \right.
  \]

  \vspace{2in}
}

\frame{
  \frametitle{Inverse transform}

  If $\Lp[x(t)]=X(s)$, then we say that
  \[
  \Lp^{-1}[X(s)] = x(t).
  \]

  We refer to $\Lp^{-1}$ as the {\color{red}\bf inverse transform.}

  \pause

  From previous discussions, we have that
  \[
  \Lp^{-1}\left[\frac{1}{s}\right] = 1,
  \]
  \[
  \Lp^{-1}\left[\frac{1}{s-a}\right] = e^{at},
  \]
  and
  \[
  \Lp^{-1}\left[\frac{1}{s^2}\right] = t.
  \]
}

\frame{
  \frametitle{Derivatives (1)}

  Let $X(s)=\Lp[x(t)]$.  We would like to compute $\Lp[x'(t)]$, i.e.,
  \[
  \int_0^\infty x'(t)e^{-st}dt.
  \]
  Using integration by parts where $u=e^{-st}$ and $v=x(t)$, we know that
  \begin{eqnarray*}
    \int_0^\infty x'(t)e^{-st}dt
    &=&
    x(t)e^{-st}\bigg\vert_0^\infty - \int_0^\infty x(t)\cdot(-s e^{-st})dt \\
    &=& -x(0)+s\int_0^\infty x(t)e^{-st}dt \\
    &=& -x(0) + s \Lp[x(t)] \\
    &=& sX(s) - x(0),
  \end{eqnarray*}
  when $s>0$.
}

\frame{
  \frametitle{Derivatives (2)}

  Let $X(s)=\Lp[x(t)]$.  We would like to compute $\Lp[x''(t)]$, i.e.,
  \[
  \int_0^\infty x''(t)e^{-st}dt.
  \]
  We can apply the formula
  \[
  \Lp[x'(t)] = sX(s) - x(0)
  \]
  to obtain that
  \[
  \Lp[x''(t)] = s^2X(s) - sx(0) - x'(0),
  \]
  when $s>0$.
  \pause

  Similarly, we have
  \[
  \Lp[x'''(t)] = s^3X(s) - s^2x(0) - sx'(0) - x''(0),
  \]
  when $s>0$.
}

\frame{
  \frametitle{Fun with derivatives}

  Note that $\frac{d}{dt}t^2=2t$.  We can compute $\Lp[(t^2)']$ in two
  ways, i.e.,
  \[
  \Lp[(t^2)'] = \Lp[2t] = 2\Lp[t] = \frac{2}{s^2},
  \]
  and, using the formula,
  \[
  \Lp[(t^2)'] = s\Lp[t^2]-0^2.
  \]
  \pause
  Thus,
  \[
  \Lp[t^2] = \frac{2}{s^3}.
  \]

  We can proceed further to obtain that
  \begin{block}{}
    \[
    \Lp[t^n] = \frac{n!}{s^{n+1}}.
    \]
  \end{block}
  
}

\frame{
  \frametitle{Other functions}

  We should get some idea on how the Laplace transform of many common
  functions can be computed.  For other functions, we can refer to the
  table.
}

\frame{

  {\bf Solving IVP: Example 1.}
  
  Let $k$ be a constant.  Consider the following IVP
  \[
  x''+k^2x=0, \ \ \ \ x(0)=0, \ \ \ \ x'(0)=1.
  \]
  \pause
  We take the Laplace transform of the function to obtain
  \[
  \Lp[x''+k^2x]=\Lp[x'']+\Lp[k^2 x]=\Lp[0]=0.
  \]
  \pause
  Recall that that if $\Lp[x]=X(s)$,
  \[
  \Lp[x''] = s^2X(s)-sx(0)-x'(0),
  \]
  then the equation becomes
  \[
  s^2X(s) - sx(0) - x'(0) - k^2X(s) = 0,
  \]
  \pause
  which solves to:
  $X(s) = \frac{1}{k^2+s^2}=\frac{1}{k}\cdot \frac{k}{s^2+k^2}.$

  \pause
  We look at the table to see that
  \[
  x(t)=\frac{1}{k}\sin kt.
  \]
}

\frame{

  {\bf Solving IVP: Example 2.}  Consider the 1st-order nonhomogeneous equation
  \[
  x'+2x=e^{-t}, \ \ \ \ x(0)=0.
  \]
  \pause
  Taking the Laplace transform yields $\Lp[x']+\Lp[2x]=\Lp[e^{-t}]$,
  or
  \[
  sX(s)-x(0)+2X(s)=1/(s+1).
  \]
  \pause
  Thus,
  \[
  X(s) = \frac{1}{(s+1)(s+2)}.
  \]
  \pause
  To obtain $x(t)$, we look at the table to see that
  \[
  x(t)
  =\Lp^{-1}\left[\frac{1}{(s+1)(s+2)}\right]
  =e^{-t}-e^{-2t}.
  \]
}

\frame{
  \frametitle{Technique: Partial fractions I}

  Consider
  \[
  \frac{1}{(s+1)(s+2)}.
  \]

  We can write
  \[
  \frac{1}{(s+1)(s+2)} = \frac{a}{s+1}+\frac{b}{s+2},
  \]
  and solve for $a$ and $b$ \pause and obtain that $a=1$ and $b=-1$, i.e.,
  \[
  \frac{1}{(s+1)(s+2)} = \frac{1}{s+1}-\frac{1}{s+2}.
  \]
  \pause

  We can then look up the inverse table for $1/(s+1)$ and $1/(s+2)$.
  
}

\frame{
  \frametitle{Technique: Partial fractions II - completing the square}

  If you cannot directly use partial fractions, you can try to modify
  the expression into the form $\frac{k}{s^2+k^2}$.
  
  Consider
  \[
  X(s) = \frac{1}{s^2+3s+6}.
  \]
  \pause

  Completing the square, we get
  \begin{eqnarray*}
    X(s) &=& \frac{1}{s^2+3s+(3/2)^2-(3/2)^2+6}\\
    &=& \frac{1}{(s^2+3/2)^2+(\sqrt{15}/2)^2} \\
    &=& \frac{2}{\sqrt{15}}
    \left(\frac{\sqrt{15}/2}{(s^2+3/2)^2+(\sqrt{15}/2)^2}\right).
  \end{eqnarray*}
  \pause

  Using the table, we conclude that $x(t) =
  \frac{2}{\sqrt{15}}e^{-3t/2}\sin\frac{\sqrt{15}}{2}t.$

}

\frame{
  \frametitle{Piecewise continuous functions: example 1 (1)}

  Consider equation
  \[
  x''+4x=\sin t - H(t-\pi)\sin t,
  \ \ \ \ \ \ \ \ \ \ \ x(0)=x'(0)=0.
  \]

  What is $\Lp[\sin t - H(t-\pi)\sin t]$?
  \pause
  \begin{eqnarray*}
    \Lp[H(t-\pi)\sin t]
    &=& e^{-\pi s}\Lp[\sin(t+\pi)] \\
    &=& e^{-\pi s}\Lp[-\sin t] \\
    &=& -e^{-\pi s}\frac{1}{s^2+1}.
  \end{eqnarray*}
  \pause

  The transformed equation becomes \pause
  \[
  X(s)=
  \frac{1}{(s^2+1)(s^2+4)}
  +
  e^{-\pi s}\frac{1}{(s^2+1)(s^2+4)}.
  \]
}

\frame{

  {\bf Solving partial fractions (1).}
  \[
  \frac{1}{(s^2+1)(s^2+4)}
  = \pause
  \frac{A}{s^2+1}+\frac{B}{s^2+4}
  = \pause
  \frac{(A+B)s^2+4A+B}{(s^2+1)(s^2+4)}.
  \]
  \pause
  Thus, $A=1/3$ and $B=-1/3$, and we get
  \[
  X(s)
  =
  \frac{1/3}{s^2+1}-\frac{1/3}{s^2+4}
  +
  e^{-\pi s}\left(\frac{1/3}{s^2+1}-\frac{1/3}{s^2+4}\right).
  \]
  \pause

  How can we get $x(t)$? \pause
  \[
  \Lp^{-1}\left[\frac{1/3}{s^2+1}\right]=\frac{1}{3}\sin t,
  \ \ \ \ \ \ \ 
  \Lp^{-1}\left[\frac{1/3}{s^2+4}\right]=\frac{1}{6}\sin 2t.
  \]
  
}

\frame{

  {\bf Solving partial fractions (2).}

  How about $e^{-\pi s}\left(\frac{1/3}{s^2+1}\right)$? \pause

  \[
  \Lp^{-1}\left[e^{-\pi s}\frac{1/3}{s^2+1}\right]=\frac{1}{3}H(t-\pi)\sin (t-\pi),
  \ \ \ \
  \Lp^{-1}\left[e^{-\pi s}\frac{1/3}{s^2+4}\right]=\frac{1}{6}H(t-\pi)\sin 2(t - \pi).
  \]

  \pause
  Finally, we get that
  \[
  x(t) = 
  \frac{1}{3}\sin t
  -\frac{1}{6}\sin 2t
  +\frac{1}{3}H(t-\pi)\sin (t-\pi)
  -\frac{1}{6}H(t-\pi)\sin 2(t - \pi).
  \]
  
}

\end{document}
