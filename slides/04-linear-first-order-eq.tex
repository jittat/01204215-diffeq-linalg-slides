\documentclass[aspectratio=169]{beamer}

\usetheme{Frankfurt}
\usepackage{graphicx}
\usepackage{epsfig}

\usefonttheme[onlymath]{serif}
\newcommand\comment[1]{}

\title{Linear First-Order Differential Equations}
\subtitle{01204215 Differential Equations and Linear Algebra}
\author{Jittat Fakcharoenphol}
\institute{Kasetsart University}
\date{\today}

\begin{document}
\frame{\titlepage}

\frame{
  \frametitle{Outline}
  \tableofcontents
}

\section{Examples}
\frame{
  \frametitle{Chemical Reactors}
  
  You have a continuously stirred tank reactor.  (In real life, it can be, e.g., a tank, a lake, a pond, or a room.)

  \begin{itemize}
  \item Suppose that the tank has a fixed volumn $V$ $m^3$.
  \item A toxic water containing a toxic chemical of constant
    concentration $C_{in}$ $g/m^3$ flows into the tank with flow rate
    $q$ $m^3/s$.
  \item At the same time, the mixed solution flows out of the tank
    with the same flow rate $q$.
  \item The initial concentration is $C_0$.  Find $C(t)$.
  \end{itemize}

  \vspace{1.5in}
}

\frame{
  \frametitle{Formulation}

  \begin{block}{}
  {\footnotesize
    \begin{itemize}
    \item A fixed volumn $V$ $m^3$.
    \item In and out flow rate $q$ $m^3/s$.
    \item In-flow {\em constant} concentration $C_{in}$ $g/m^3$
    \item The initial concentration is $C_0$.  Find $C(t)$.
    \end{itemize}
  }
  \end{block}
  \vspace{2.5in}
}

\frame{
  \frametitle{Different flow rates: formulation}
  \begin{block}{}
  \begin{itemize}
  \item A starting volumn $V_0$ $m^3$.
  \item In-flow rate $q_{in}$ $m^3/s$, out-flow rate $q_{out}$ $m^3/s$.
  \item In-flow {\em constant} concentration $C_{in}$ $g/m^3$
  \item The initial concentration is $C_0$.  Find $C(t)$.
  \end{itemize}
  \end{block}

  \pause
  Volume: $V(t)=V_0+(q_{in}-q_{out})t$, i.e., $V'=q_{in} - q_{out}$.

  \pause
  Changes in quantity: $(VC)'=q_{in}C_{in}-q_{out}C$
  \pause

  Thus,
  \[
  (VC)' = V'C + VC' = (q_{in} - q_{out})C+VC'= q_{in}C_{in}-q_{out}C.
  \]
  \pause

  Rearranging, we get
  \[
  C' + \frac{q_{in}}{V}C = \frac{q_{in}C_{in}}{V}.
  \]
}

\frame{
  \frametitle{Spendng money}

  A household has money $M=M(t)$ at time $t$.  Initially at time $0$,
  they have $m_0$. They spend money proportionally to what they
  currently have, i.e., they spend money at the rate of $aM$.  Their
  income at time $t$ is $I(t)$.

  \vspace{2in}

}

\section{Solving linear first-order DE}
\frame{
  \frametitle{Linear differential equations}

  A differential equation
  \[
  f_n(t)x^{(n)} + 
  f_{n-1}(t)x^{(n-1)} + \cdots +
  f_{3}(t)x''' +
  f_{2}(t)x'' + 
  f_{1}(t)x' +
  f_0(t) = 0
  \]
  is called a {\em linear} differential equation.

  It is linear in the sense that each $x, x', x'', \ldots$ depends
  {\bf linearly} on each other.

}

\frame{
  \frametitle{Linear First-Order DE}

  We are interested in a {\em linear} differential equation in the
  form:

  \[
  x' + p(t)x = q(t).
  \]
}

\frame{
  \frametitle{Review: separable equations}

  Consider the equation
  \[
  \frac{dx}{dt}=2xt.
  \]
  To solve it, intuitively we need to ``get rid of'' the ``$d$'' in
  the equation.

  \pause

  \vspace{1.5in}

  And we reach equation
  \[
  \int \frac{1}{x} dx = \int 2t dt + C.
  \]

}

\frame{
  \frametitle{Hint: the product rule}
  From
  \[
  x' + p(t)x = q(t),
  \]
  \pause
  if we can find $\mu(t)$ such that
  \[
  \mu(t)(x' + p(t)x) = (M(t)\cdot x)',
  \]
  for some $M(t)$, when we can just integrate both sides and get
  \[
  \int \frac{d}{dt} (M(t)\cdot x) dt = \int \mu(t)q(t) dt + C,
  \]
  that would lead to the solution.
}

\frame{
  \frametitle{Integrating factors}
  We want $\mu(t)$ such that \pause
  \[
  \frac{d}{dt}\mu(t) = p(t)\mu(t).
  \]
  Easy case: when $p(t)=1$, what should $\mu(t)$ be?
  \vspace{0.5in}

  \pause
  General case:
  \[
  \mu(t) = e^{\int p(t) dt},
  \]
  or $\mu(t) = e^{P(t)}$, where $P(t) = \int p(t)dt$.
}

\frame{
  Let's make sure that we have the right formula:

  \begin{eqnarray*}
    \frac{d}{dt}\left(\mu(t)\cdot x\right)
    &=&
    \mu(t)\cdot \frac{d}{dt} x
    +
    \left(\frac{d}{dt}\mu(t)\right)\cdot x\\
    \pause
    &=&
    \mu(t)\cdot \frac{d}{dt} x
    +
    \left(\frac{d}{dt}e^{\int p(t)dt}\right)\cdot x\\
    \pause
    &=&
    \mu(t)\cdot \frac{d}{dt} x
    +
    e^{\int p(t)dt}\left(\frac{d}{dt} \int p(t)dt\right)\cdot x\\
    \pause
    &=&
    \mu(t)\cdot \frac{d}{dt} x
    +
    \mu(t)\cdot p(t)\cdot x\\
    &=&
    \mu(t)\cdot x'
    +
    \mu(t)\cdot p(t)\cdot x
  \end{eqnarray*}
}

\frame{
  \frametitle{Steps to solve first-order linear differential equations}
}

\frame{
  \frametitle{Example 1}
  Solve the equation $x'+\frac{1}{t}x = 1$.
  \vspace{2in}
}

\frame{
  \frametitle{Example 2 - Sales Response to Advertising}
  Let $S=S(t)$ be the monthly sales of an item.
  \begin{itemize}
  \item Sales decay over time: $S'=-aS$.
  \item Market size that can be saturated by advertisement is $M$.
  \item If we apply advertising at rate $A(t)$, the amount of sales
    grows proportionally to the untapped market, i.e., the rate is
    $
    r\cdot A(t)\left(\frac{M-S}{M}\right).
    $
  \item Combine both terms we get
    \[
    S'=-aS+rA(t)\left(\frac{M-S}{M}\right).
    \]
  \end{itemize}
}

\frame{
  \[
  S'=-aS+rA(t)\left(\frac{M-S}{M}\right).
  \]
  \vspace{3in}
}

\frame{
  \frametitle{Breaking down: $x'+p(t)\cdot x = q(t)$}
  \begin{itemize}
  \item When $q(t)=0$, we have the equation
    \[
    x'+p(t)x = 0,
    \]
    that solves to \pause $x(t) = Ce^{-\int p(t)dt}$.  An equation in
    this form is called a {\bf homogeneous} equation.
  \item When $q(t)\neq 0$, it is referred to as the {\bf forcing term}
    or {\bf source term}.
  \end{itemize}
}

\frame{
  \frametitle{Homogeneous and particular solutions}
  The general solution to equation
  \[
  x'+p(t)x = q(t),
  \]
  using the integrating factor is
  \[
  Ce^{-P(t)} + e^{-P(t)}\int q(t)e^{P(t)} dt,
  \]
  where $P(t)=\int p(t)dt$.
}

\end{document}
