\include{commons}
\lecturetitle{Lecture 10a: Nullspaces}

\begin{frame}
  \frametitle{Review: Linear combinations}

  \begin{block}{Definition}
    For any scalars
    \[
    \alpha_1,\alpha_2,\ldots,\alpha_m
    \]
    and vectors
    \[
    \uv_1,\uv_2,\ldots,\uv_m,
    \]
    we say that
    \[
    \alpha_1\uv_1 + \alpha_2\uv_2 + \cdots + \alpha_m \uv_m
    \]
    is a \textcolor{red}{\bf linear combination} of $\uv_1,\ldots,\uv_m$.
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Review: Span}

  \begin{block}{Definition}
    A set of all linear combination of vectors $\uv_1,\uv_2,\ldots,\uv_m$ is called the \textcolor{red}{\bf span} of that set of vectors.

    It is denoted by $\mathrm{Span} \{\uv_1,\uv_2,\ldots,\uv_m\}$.
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Review: Vector spaces}
  \begin{block}{Definition}
    A set $\V$ of vectors over $\ff$ is a \textcolor{red}{\bf vector space} iff
    \begin{itemize}
    \item \textcolor{blue}{(V1)} $\vect{0}\in\V$,
    \item \textcolor{blue}{(V2)} for any $\uv\in\V$,
      \[
      \alpha\cdot\uv\in\V
      \]
      for any
      $\alpha\in\ff$, and
    \item \textcolor{blue}{(V3)} for any $\uv,\vv\in\V$,
      \[
      \uv+\vv\in\V.
      \]
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Review: Linearly independence}

  \begin{block}{Definition}
    Vectors $\uv_1,\uv_2,\ldots,\uv_n$ are \textcolor{red}{\bf
      linearly independent} if no vector $\uv_i$ can be written as a
    linear combination of other vectors.
  \end{block}

  \begin{block}{(Another) Definition}
    Vectors $\uv_1,\uv_2,\ldots,\uv_n$ are \textcolor{red}{\bf
      linearly independent} if the only solution of equation
    \[
    \alpha_1\uv_1 + \alpha_2\uv_2 +\cdots+\alpha_n\uv_n = \vect{0}
    \]
    is
    \[
    \alpha_1=\alpha_2=\cdots=\alpha_n=0.
    \]
  \end{block}

\end{frame}
\begin{frame}
  \frametitle{Review: Bases}

  \begin{block}{Definition}
    A set of vectors $\{\uv_1,\uv_2,\ldots,\uv_k\}$ is a \textcolor{red}{\bf basis} for vector space $\V$ if
    \begin{itemize}
    \item $\mathrm{Span}~\{\uv_1,\uv_2,\ldots,\uv_k\} = \V$, and
    \item $\uv_1,\uv_2,\ldots,\uv_k$ are linearly independent.
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Review: Dimensions}

  \begin{block}{Definition}
    The \textcolor{red}{\bf dimension} of a vector space $\V$ is the size of its basis.
    
    The dimension of $\V$ is written as $\mathrm{dim}~\V$.
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{Review: Four fundamental subspaces}

  \begin{block}{Four fundamental subspaces}
    Given an $m$-by-$n$ matrix $A$, we have the following subspaces
    \begin{itemize}
    \item The column space of $A$ (denoted by ${\mathcal C}(A)$
      {$\subseteq \rf^m$}
      )
    \item The row space of $A$ (denoted by ${\mathcal C}(A^T)$
      {$\subseteq \rf^n$}
      )
    \item The nullspace of $A$
      \[
        {\mathcal N}(A) = \{\vect{x} \;|\; A\vect{x}=\vect{0}\}
        {\subseteq \rf^n}
      \]
    \item The left nullspace of $A$
      \[
        {\mathcal N}(A^T) = \{\vect{y} \;|\; A^T\vect{y}=\vect{0}\}
        {\subseteq \rf^m}
      \]
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Review: Ranks}

  \begin{block}{Definition}
    Consider an $m$-by-$n$ matrix $A$.
    \begin{itemize}
    \item The \textcolor{red}{\bf row rank} of $A$ is the maximum
      number of linearly independent rows of $A$.
    \item The \textcolor{red}{\bf column rank} of $A$ is the maximum
      number of linearly independent columns of $A$.
    \end{itemize}
  \end{block}

  \vspace{0.2in}

  {\bf Remark:} The column rank of $A$ is $\dim {\mathcal R}(A)$.  The
  row rank of $A$ is $\dim {\mathcal R}(A^T)$.
\end{frame}

\frame{
  \frametitle{Ranks and nullspaces}

  Given an $m\times n$-matrix $A$ with rank $r$, we know that
  \begin{itemize}
  \item Dimensions of row spaces ${\mathcal R}(A^T)$ and column spaces
    ${\mathcal R}(A)$ are $r$.
    \pause
  \item What can we say about ${\mathcal N}(A)$?
  \end{itemize}
}

\frame{
  \frametitle{Finding the rank of a matrix}

  \begin{itemize}
  \item How can we find the rank of $A$? \pause
  \item We can use Gaussian Elimination to reduce $A$ to matrix $U$ in
    the echelon form.

    \vspace{1in}
    \pause

  \item What can we say about the row spaces and and nullspaces of $A$
    and $U$?
  \end{itemize}
}

\frame{
  \begin{itemize}
  \item Suppose that $U$ has $r$ non-zero rows.
  \item Look at the structure of $U$.  \pause The rank of $U$ is $r$.
  \item We construct $U$ from $A$ using row operations. \pause We
    cannot increase row span. \pause Thus, the rank of $U$ is at most
    the rank of $A$. \pause
  \item On the flip side, every operation performed to get $U$ from
    $A$ is reversible, i.e., we can reconstruct $A$ from $U$ using row
    operation as well. \pause Thus, the rank of $A$ is also at most
    $U$. \pause
  \item The number of non-zero rows in $U$ is the rank of $A$.
  \end{itemize}
}

\frame{
  \frametitle{Matrices $A$ and $U$}

  \[
  \begin{bmatrix}
    * & * & * & * & * & * & * \\
    * & * & * & * & * & * & * \\
    * & * & * & * & * & * & * \\
    * & * & * & * & * & * & * \\
    * & * & * & * & * & * & * \\
    * & * & * & * & * & * & * \\
  \end{bmatrix}
  \Longrightarrow
  \begin{bmatrix}
    * & * & * & * & * & * & * \\
    0 & * & * & * & * & * & * \\
    0 & 0 & 0 & * & * & * & * \\
    0 & 0 & 0 & 0 & * & * & * \\
    0 & 0 & 0 & 0 & 0 & 0 & * \\
    0 & 0 & 0 & 0 & 0 & 0 & 0 \\
  \end{bmatrix}
  \]

  \vspace{0.2in}
  
  \pause

  Think about $A\vect{x}=\vect{0}$ and $U\vect{x}=\vect{0}$.
  
  \pause

  There are $r$ pivots.

  Basic variables: \\
  Free variables: \\
}

\frame{
  \frametitle{Example}

  Consider this system of linear equations
  \[
  \begin{bmatrix}
    1 & 3 & 4 & 2 \\
    3 & 9 & 15 & 7 \\
    4 & 12 & 19 & 9 \\
    5 & 15 & 23 & 11 \\
  \end{bmatrix}
  \begin{bmatrix}
    w \\ x \\ y \\ z
  \end{bmatrix}
  =
  \begin{bmatrix} 0 \\ 0 \\ 0 \\ 0 \end{bmatrix}
  \]

  \pause

  After Gaussian Elimination:
  \[
  \begin{bmatrix}
    1 & 3 & 4 & 2 \\
    0 & 0 & 3 & 1 \\
    0 & 0 & 0 & 0 \\
    0 & 0 & 0 & 0 \\
  \end{bmatrix}
  \begin{bmatrix}
    w \\ x \\ y \\ z
  \end{bmatrix}
  =
  \begin{bmatrix} 0 \\ 0 \\ 0 \\ 0 \end{bmatrix}
  \]
  \pause

  We can write the solutions (with $z$ and $x$ as free variables) as:
  \[
  y=-z/3,
  \]
  \[
  w=-3x-4y-2z=-3x+4z/3-2z=3x-2z/3,
  \]
}  

\frame{
  \frametitle{Solutions}

  Solution to the previous system of linear equations
  $A\vect{x}=\vect{0}$ is
  \[
  \begin{bmatrix}
    3x-2z/3 \\
    x \\
    -z/3 \\
    z
  \end{bmatrix}
  \pause
  =
  x\cdot\begin{bmatrix}
  3 \\ 1 \\ 0 \\ 0
  \end{bmatrix}
  +
  z\cdot\begin{bmatrix}
  -2/3 \\ 0 \\ -1/3 \\ 1
  \end{bmatrix}
  \]

  Thus, vector space ${\mathcal N}(A)$ is spanned by two linearly
  indepedent vectors $[3,1,0,0]$ and $[-2/3,0,-1/3,0]$. \pause

  Thus,
  \[
  \mathrm{dim}~{\mathcal N}(A) = 2.
  \]
}

\frame{
  \frametitle{Matrices $A$ and $U$}

  \[
  \begin{bmatrix}
    * & * & * & * & * & * & * \\
    * & * & * & * & * & * & * \\
    * & * & * & * & * & * & * \\
    * & * & * & * & * & * & * \\
    * & * & * & * & * & * & * \\
    * & * & * & * & * & * & * \\
  \end{bmatrix}
  \Longrightarrow
  \begin{bmatrix}
    * & * & * & * & * & * & * \\
    0 & * & * & * & * & * & * \\
    0 & 0 & 0 & * & * & * & * \\
    0 & 0 & 0 & 0 & 0 & * & * \\
    0 & 0 & 0 & 0 & 0 & 0 & 0 \\
    0 & 0 & 0 & 0 & 0 & 0 & 0 \\
  \end{bmatrix}
  \]

  \vspace{0.2in}
  

  Think about $A\vect{x}=\vect{0}$ and $U\vect{x}=\vect{0}$.
  
  There are $r$ pivots.

  Basic variables: \pause $r$ \pause\\
  Free variables: \pause $n-r$ \pause\\
  Thus, $\mathrm{dim}~{\mathcal N}(A) = n-r.$
}

\begin{frame}
  \frametitle{Dimensions}

  \begin{block}{Four fundamental subspaces}
    Given an $m$-by-$n$ matrix $A$ of rank $r$, we have the following
    subspaces
    \begin{itemize}
    \item The column space of $A$ (denoted by ${\mathcal R}(A)$
      {$\subseteq \rf^m$})

      $\dim~{\mathcal R}(A) = r$.
    \item The row space of $A$ (denoted by ${\mathcal R}(A^T)$
      {$\subseteq \rf^n$})

      $\dim~{\mathcal R}(A^T) = r$.
    \item The nullspace of $A$ (denoted by ${\mathcal N}(A) \subseteq \rf^n$)

      $\dim~{\mathcal N}(A) = n-r$.
    \item The left nullspace of $A$ (denoted by ${\mathcal N}(A^T) \subseteq \rf^m$)

      $\dim~{\mathcal N}(A) = m-r$.
    \end{itemize}
  \end{block}
\end{frame}
